## Code style

### Python
PEP8 is the preferred code style.

The following tools are used for linting and formatting:
- ruff: linter
- ruff: formatter
- ruff: sort imports

Style configurations of ruff are in *pyproject.toml*.

Pre-commit hook is used for checks, formats and import sorts on every commit.  
The config for a pre-commit hook is in *.pre-commit-config*.
