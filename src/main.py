import os

import pandas as pd


def test():
    df = pd.DataFrame(
        zip(
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
            "qwertyuiopasdf",
        )
    )
    print(len(df))


def main():
    print(f"{os.getcwd()}")
    test()


if __name__ == "__main__":
    main()
